# Orange On site Test

## Requirements:
-- Python 3

## INSTALLMENT
for easier use please put the zip file in data folder.

```bash
pip install -r requirements.txt
```


## Project Description.

The project is to read a .zip folder with the name from config/constants_ENV.py ex (ENV_FOLDER_NAME="ml-100k.zip") 

after you read the data, extract the most popular movies.

Results will be printed to csv file in data folder, file can be opened by any editor.


