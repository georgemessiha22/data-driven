def read_from_zip():
    """
    This method reads data from ENV_FOLDER_NAME configured in config_ENV
    :return: DataFrame read.
    """
    from config.constants import ZIP_FOLDER_NAME, U_DATA_FILE_TO_READ, U_ITEM_FILE_TO_READ, U_DATA, U_ITEM
    import zipfile
    import pandas as pd
    u_data = None
    u_item = None
    with zipfile.ZipFile(ZIP_FOLDER_NAME) as z:
        with z.open(U_DATA_FILE_TO_READ) as f:
            u_data = pd.read_csv(f, sep="\t", names=U_DATA)
        with z.open(U_ITEM_FILE_TO_READ) as f:
            u_item = pd.read_csv(f, sep='|', encoding="ISO-8859-1", names=U_ITEM)

    return u_data, u_item
