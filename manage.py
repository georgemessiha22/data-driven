#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    try:
        from management.core import execute
    except ImportError:
        raise ImportError(
            "Could not import module management please refer to support"
        )
    execute()
