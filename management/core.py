def execute_from_command_line(command):
    pass


def execute():
    from reader.read_zip import read_from_zip
    df_1, df_2 = read_from_zip()
    most_popular = find_most_popular_movie(u_data=df_1, u_item=df_2)
    print_selected_movies(movies=df_2, selected=most_popular)


def find_most_popular_movie(u_data, u_item):
    import pandas as pd
    from config.constants import U_DATA, MOVIE_ID, RATING, MOVIE_TITLE, MEAN_RATING, NUM_RATING, BE
    df = pd.merge(u_data, u_item, on=MOVIE_ID)
    total_rating = u_data[RATING].mean()
    top_movies = pd.DataFrame()
    top_movies[MEAN_RATING] = df.groupby(MOVIE_ID)[RATING].mean()
    top_movies[NUM_RATING] = df.groupby(MOVIE_ID)[RATING].count()
    top_movies[BE] = (top_movies[NUM_RATING] * top_movies[MEAN_RATING] + 100 * total_rating) / (
                top_movies[NUM_RATING] + 100)
    top_movies.reset_index(level=0, inplace=True)
    return top_movies


def print_selected_movies(movies, selected):
    import pandas as pd
    from config.constants import MOVIE_ID, BE
    # print(selected.info())
    df = pd.merge(movies, selected, on=MOVIE_ID)
    sorted_df = df.sort_values([BE], ascending=False)
    sorted_df.to_csv("data/out.csv", sep="|")
